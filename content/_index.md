---
title: "/home/andy"
date: 2022-12-28T19:44:42+01:00
description: "I’m Andy Sukowski-Bang, studying pure mathematics and computer science at the Kiel University (Germany), with a particular interest in algebraic topology, machine learning, cryptography and decentralized networking. In my free time I go bouldering, play electric guitar or develop software in Julia, Haskell and C (check out my GitLab)."
---

I’m Andy Sukowski-Bang, studying pure mathematics and computer science at the
[Christian-Albrecht University of Kiel][1] (Germany), with a particular interest in
algebraic topology, machine learning, cryptography and decentralized
networking. In my free time I go bouldering, play electric guitar or develop
software in Julia, Haskell and C (check out [my résumé][2] and [my GitLab][3]).

{{< social-icons style="margin-top: 1.6rem;" >}}

## My Projects

I have uploaded most of my personal projects to [my GitLab][3] and mirrored
them to [my GitHub][4]. These are some of my favorite repositories. If you like
what you see and are absolutely dying to [send me some Monero (XMR)][5], feel
free.

{{< repository href="https://git.andy-sb.com/nn" src="/img/repo/nn.webp" name="Neural Network Library" desc="Extensible neural network library with LSTM, convolutional, pooling and dense layer types, written from scratch in Julia." >}}
{{< repository href="https://git.andy-sb.com/mlp" src="/img/repo/mlp.webp" name="Multilayer Perceptron" desc="Simple multilayer perceptron (MLP), also known as a fully connected feedforward artificial neural network, written from scratch in Julia." >}}
{{< repository href="https://git.andy-sb.com/ffn" src="/img/repo/ffn.webp" name="Fourier Feature Network" desc="n-dimensional Fourier feature network, written in Julia using Flux.jl." >}}
{{< repository href="https://git.andy-sb.com/steg" src="/img/repo/steg.webp" name="Steganography" desc="Steganography tool enabling you to conceal any file within a PNG file." >}}
{{< repository href="https://git.andy-sb.com/calc" src="/img/repo/calc.webp" name="Calculator" desc="Arithmetic calculator, which parses mathematical expressions specified in infix notation using the shunting-yard algorithm." >}}
{{< repository href="https://git.andy-sb.com/brain" src="/img/repo/brain.webp" name="Brainfuck Interpreter" desc="Simple interpreter for the brainfuck programming language, written in C." >}}
{{< repository href="https://git.andy-sb.com/waves" src="/img/repo/waves.webp" name="Wave Simulator" desc="n-dimensional wave simulator, written in Julia." >}}

## My Linux Setup

A few years ago, I completely wiped Windows from my SSD, replacing it with
Ubuntu. Over the years, I've distrohopped to Arch, Gentoo (took way too long to
compile programs) and finally to Void Linux. I've used Gnome, KDE, qtile, i3 and
now DWM from [suckless.org][6]. My workflow is primarily command-line based, so I
frequently use Neovim, ffmpeg, ImageMagick, GnuPG, [pass][7] and the *nix coreutils.

{{< image src="/img/linux_system.webp" alt="Void Linux with DWM, LunarVim, HTOP and neofetch, Gruvbox Material theme" desc="Void Linux with DWM using Gruvbox Material" >}}

[1]: https://www.uni-kiel.de
[2]: /cv.pdf
[3]: https://gitlab.com/andy-sb
[4]: https://github.com/andy-sukowski
[5]: /xmr
[6]: https://suckless.org
[7]: https://www.passwordstore.org/
